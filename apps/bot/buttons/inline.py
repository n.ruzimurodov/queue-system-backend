from datetime import datetime

from django.db.models import Q, Count, F
from django.utils import timezone
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from apps.common.models import Region
from django.conf import settings
from django.utils.translation import gettext_lazy as _

from apps.organization.models import Organization, Service


def region_buttons():
    regions = Region.objects.all()
    buttons = []
    for region in regions:
        buttons.append([InlineKeyboardButton(region.title, callback_data=f"region_{region.id}")])
    return InlineKeyboardMarkup(buttons)


def district_buttons(region_id):
    districts = Region.objects.get(id=region_id).districts.all()
    buttons = []
    for district in districts:
        buttons.append([InlineKeyboardButton(district.title, callback_data=f"district_{district.id}")])
    return InlineKeyboardMarkup(buttons)


def language_buttons():
    buttons = []
    languages = settings.LANGUAGES
    for language in languages:
        buttons.append([InlineKeyboardButton(str(_(language[1])), callback_data=f"language_{language[0]}")])
    return InlineKeyboardMarkup(buttons)


def organization_buttons_by_region(region_id):
    buttons = []
    organizations = Organization.objects.filter(district__region_id=region_id)
    for organization in organizations:
        buttons.append([InlineKeyboardButton(organization.title, callback_data=f"organization_{organization.id}")])
    return InlineKeyboardMarkup(buttons)


def organization_services_buttons(organization_id):
    buttons = []
    organization = Organization.objects.get(id=organization_id)
    for service in organization.services.all():
        buttons.append([InlineKeyboardButton(service.title, callback_data=f"service_{service.id}")])
    return InlineKeyboardMarkup(buttons)


def day_buttons():
    buttons = []
    days = [i for i in range(7)]
    for day in days:
        # day in words like "Monday"
        day_in_words = (timezone.now().date() + timezone.timedelta(days=day)).strftime('%A')
        # date_in_words like "12.12.2021"
        date_in_words = (timezone.now().date() + timezone.timedelta(days=day)).strftime('%d.%m.%Y')
        buttons.append([InlineKeyboardButton(day_in_words + f"  ({date_in_words})", callback_data=f"date_{date_in_words}")])
    return InlineKeyboardMarkup(buttons)


def date_time_buttons(service: Service, date: datetime.date):
    buttons = []
    week_day = date.weekday()
    service_times = service.working_times.annotate(
        d_count=Count('service_queues')
    ).annotate(
        number_of_free_places=F('number_of_clients') - F('d_count')
    ).filter(day=week_day, number_of_free_places__gt=0)

    for service_time in service_times:
        buttons.append(
            [InlineKeyboardButton(service_time.start_time.strftime(
                '%H:%M') + " - " + service_time.end_time.strftime(
                '%H:%M') + f" ({service_time.number_of_free_places} free places)",
                                  callback_data=f"datetime_{service_time.id}")])
    return InlineKeyboardMarkup(buttons)
