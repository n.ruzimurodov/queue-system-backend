import datetime
import logging

import telegram
from django.conf import settings
from django.core.exceptions import BadRequest
from django.utils import timezone
from telegram import Update, ReplyKeyboardRemove
from telegram.ext import CallbackContext
from django.utils.translation import gettext_lazy as _, activate
from apps.bot import models
from utils.decarators import get_member
from .buttons.inline import region_buttons, district_buttons, language_buttons, organization_buttons_by_region, \
    organization_services_buttons, day_buttons, date_time_buttons
from .state import state
from ..organization.models import Organization, Service, ServiceQueue

logger = logging.getLogger(__name__)


@get_member
def start(update: Update, context: CallbackContext, tg_user: models.TelegramProfile):
    if tg_user.language is None:
        message = str(_("""Assalomu alaykum Yaxshimisiz {} \n
    O'zingiz uchun qulay bo'lgan tilni tanlang""")).format(tg_user.first_name)
        update.message.reply_html(
            text=message,
            reply_markup=language_buttons()
        )
        return state.LANGUAGE
    else:
        update.message.reply_html(
            text=str(_("""Quyidagi viloyatlardan birini tanlang""")).format(tg_user.first_name),
            reply_markup=region_buttons()
        )
        return state.REGION


@get_member
def get_language(update: Update, context: CallbackContext, tg_user: models.TelegramProfile):
    query = update.callback_query
    language = query.data
    tg_user.language = language
    tg_user.save()
    activate(language)
    query.edit_message_text(
        text=str(_("""Quyidagi viloyatlardan birini tanlang""")).format(tg_user.first_name),
        reply_markup=region_buttons()
    )
    return state.REGION


@get_member
def get_region(update: Update, context: CallbackContext, tg_user: models.TelegramProfile):
    query = update.callback_query
    text, region_id = query.data.split('_')
    if text != 'region':
        return
    query.edit_message_text(
        text=str(_("""Ushbu hududdagi tashkilotlardan birini tanlang.""")),
        reply_markup=organization_buttons_by_region(region_id)
    )
    return state.ORGANIZATION


@get_member
def get_organization(update: Update, context: CallbackContext, tg_user: models.TelegramProfile):
    query = update.callback_query
    text, organization_id = query.data.split('_')
    if text != 'organization':
        return
    organization = Organization.objects.get(id=organization_id)
    message = str(_("""{} tashkiloti haqida ma'lumot\n{}\n
tashkilotning quyidagi servicelaridan birini tanlang""")).format(organization.title, organization.description)
    if organization and organization.logo:
        try:
            query.message.delete()
            query.bot.send_photo(
                chat_id=query.message.chat_id,
                photo=open(organization.logo.path, 'rb'),
                caption=message,
                reply_markup=organization_services_buttons(organization_id)
            )
        except BadRequest:
            query.bot.send_message(
                chat_id=query.message.chat_id,
                text=message,
                reply_markup=organization_services_buttons(organization_id)
            )
    else:
        query.edit_message_text(
            text=message,
            reply_markup=organization_services_buttons(organization_id)
        )

    return state.ORGANIZATION_SERVICES


@get_member
def get_organization_services(update: Update, context: CallbackContext, tg_user: models.TelegramProfile):
    query = update.callback_query
    text, service_id = query.data.split('_')
    if text != 'service':
        return
    service = Service.objects.get(id=service_id)
    context.user_data['service_id'] = service_id
    message = str(_("""{} tashkilotining {} xizmati haqida ma'lumot\n{}\n 
Ushbu servicega yozilish uchun quyidagi kunlardan birini tanlang""".format(
        service.organization.title, service.title, service.description)))
    if service and service.image:
        try:
            query.message.delete()
            query.bot.send_photo(
                chat_id=query.message.chat_id,
                photo=open(service.image.path, 'rb'),
                caption=message,
                reply_markup=day_buttons()
            )
        except BadRequest:
            query.bot.send_message(
                chat_id=query.message.chat_id,
                text=message,
                reply_markup=day_buttons()
            )
    else:
        query.edit_message_text(
            text=message,
            reply_markup=day_buttons()
        )
    return state.ORGANIZATION_SERVICES_DATE


@get_member
def get_organization_services_date(update: Update, context: CallbackContext, tg_user: models.TelegramProfile):
    query = update.callback_query
    text, date = query.data.split('_')
    if text != 'date':
        return
    service = Service.objects.get(id=context.user_data['service_id'])
    date = timezone.datetime.strptime(date, '%d.%m.%Y').date()
    queue = ServiceQueue.objects.filter(
        date=date,
        profile=tg_user,
        service=service
    ).first()
    if queue:
        query.answer(
            text=str(_("""Siz ushbu kun uchun {} xizmatiga yozilgansiz\n
Sizning raqamingiz: {}\n
iltimos boshqa sanani kunni tanlang """)).format(service.title, queue.id),
            show_alert=True
        )
        return state.ORGANIZATION_SERVICES_DATE
    context.user_data['date'] = date
    if not service.is_available_on_date(date):
        query.answer(
            text=str(_("""Ushbu kun uchun {} xizmati mavjud emas\n
iltimos boshqa sanani kunni tanlang""")).format(service.title),
            show_alert=True
        )
        return state.ORGANIZATION_SERVICES_DATE
    query.message.delete()
    query.message.reply_html(
        text=str(_("""{} xizmati {} sanasiga yozilish uchun 
o'zingiz uchun qulay bo'lgan vaqtni tanlang """)).format(
            service.title, date),
        reply_markup=date_time_buttons(service=service, date=date)
    )
    return state.ORGANIZATION_SERVICES_TIME


@get_member
def get_service_time(update: Update, context: CallbackContext, tg_user: models.TelegramProfile):
    query = update.callback_query
    text, service_time_id = query.data.split('_')
    query.message.delete()
    if text != 'datetime':
        return
    service = Service.objects.get(id=context.user_data['service_id'])
    service_time = service.working_times.get(id=service_time_id)
    queue = ServiceQueue.objects.create(
        service_time=service_time,
        profile=tg_user,
        date=context.user_data['date'],
        service_time_id=int(service_time_id),
        time=service_time.start_time,
        service=service
    )
    query.message.reply_html(
        text=str(_("""{} xizmati {} sanasiga {} vaqti uchun yozilish muvaffaqiyatli amalga oshirildi.\n
Sizning raqamingiz: {}\n
Boshqa servicelarga yozilish uchun quyidagi viloyatlardan birini tanlang
""")).format(
            service.title, context.user_data['date'], service_time.start_time.strftime(
                '%H:%M'), queue.id),
        reply_markup=region_buttons()
    )
