import json
import os
from queue import Queue

from django.conf import settings
from django.http import JsonResponse
from .state import state
from telegram import Update, Bot
from telegram.ext import Dispatcher, CommandHandler, ConversationHandler, PicklePersistence, CallbackQueryHandler
from .telegrambot import start, get_language, get_organization, get_region, get_organization_services, \
    get_organization_services_date, get_service_time


def setup(token):
    bot = Bot(token=token)
    queue = Queue()
    # create the dispatcher
    if not os.path.exists(os.path.join(settings.BASE_DIR, "media", "state_record")):
        os.makedirs(os.path.join(settings.BASE_DIR, "media", "state_record"))
    dp = Dispatcher(bot, queue, workers=4, use_context=True, persistence=PicklePersistence(
        filename=os.path.join(
            settings.BASE_DIR, "media", "state_record", "conversationbot"
        )
    ),  # to store member state
                    )

    states = {
        state.START_POINT: [CommandHandler('start', start)],
        state.LANGUAGE: [
            CallbackQueryHandler(get_language),
        ],
        state.ORGANIZATION: [
            CallbackQueryHandler(get_organization),
        ],
        state.REGION: [
            CallbackQueryHandler(get_region),
        ],
        state.ORGANIZATION_SERVICES: [
            CallbackQueryHandler(get_organization_services),
        ],
        state.ORGANIZATION_SERVICES_DATE: [
            CallbackQueryHandler(get_organization_services_date),
        ],
        state.ORGANIZATION_SERVICES_TIME: [
            CallbackQueryHandler(get_service_time),
        ],

    }
    entry_points = [CommandHandler('start', start)]
    fallbacks = [CommandHandler('start', start)]
    conversation_handler = ConversationHandler(
        entry_points=entry_points,
        states=states,
        fallbacks=fallbacks,
        persistent=True,
        name="conversationbot",
    )
    dp.add_handler(conversation_handler)
    return dp


def handle_telegram_webhook(request):
    token = settings.BOT_TOKEN
    bot = Bot(token=token)
    update = Update.de_json(json.loads(request.body.decode('utf-8')), bot)
    dp = setup(token)
    try:
        if update.message.chat.type == 'private':
            dp.process_update(update)
    except Exception as e:
        dp.process_update(update)
    return JsonResponse({'status': 'ok'})
