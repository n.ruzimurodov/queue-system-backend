import requests.exceptions
from django.apps import AppConfig
from django.conf import settings
import telegram
import django.core.exceptions
from django.urls import reverse


class BotConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.bot'

    def ready(self):
        try:
            bot_token = settings.BOT_TOKEN
            host = settings.HOST
            bot = telegram.Bot(token=bot_token)
            webhook_url = host + reverse('telegram_webhook')
            bot.set_webhook(webhook_url)
            print("Webhook set successfully for bot: {} to {} webhook url".format(bot.get_me().username,
                                                                               webhook_url))
        except telegram.error.RetryAfter:
            pass
        except requests.exceptions.ConnectionError:
            print("Connection error. Please check your internet connection")
        except django.core.exceptions.ImproperlyConfigured:
            print("Improperly configured. Please check your settings")
        except telegram.error.Unauthorized:
            print("Unauthorized. Please check your bot token")
