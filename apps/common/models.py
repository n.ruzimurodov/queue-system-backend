from django.db import models
from django.utils.translation import gettext_lazy as _


class TimeStampedModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class BaseModel(TimeStampedModel):
    is_active = models.BooleanField(default=True, verbose_name=_("Is active"))

    class Meta:
        abstract = True


class Region(BaseModel):
    title = models.CharField(max_length=255, verbose_name=_("Region title"))

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _("Region")
        verbose_name_plural = _("Regions")
        db_table = "region"


class District(BaseModel):
    region = models.ForeignKey(Region, on_delete=models.CASCADE, verbose_name=_("Region"))
    title = models.CharField(max_length=255, verbose_name=_("District title"))

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _("District")
        verbose_name_plural = _("Districts")
        db_table = "district"