from django.contrib import admin
from .models import Organization, OrganizationImage, ServiceWorkingTime, ServiceQueue, Service


@admin.register(Organization)
class OrganizationAdmin(admin.ModelAdmin):
    pass


@admin.register(OrganizationImage)
class OrganizationImageAdmin(admin.ModelAdmin):
    pass


@admin.register(ServiceWorkingTime)
class ServiceWorkingTimeAdmin(admin.ModelAdmin):
    pass


@admin.register(ServiceQueue)
class ServiceQueueAdmin(admin.ModelAdmin):
    list_display = ['id', 'service', 'profile', 'date', "service_time", "is_cancelled"]
    list_filter = ['service__organization']


@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    pass
