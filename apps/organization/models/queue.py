from django.db import models
from apps.common.models import BaseModel
from .service import Service
from django.utils.translation import gettext_lazy as _
from apps.bot.models import TelegramProfile
from .service import ServiceWorkingTime


class ServiceQueue(BaseModel):
    service = models.ForeignKey(Service, on_delete=models.CASCADE, verbose_name=_("Service"),
                                related_name="service_queues")
    profile = models.ForeignKey(TelegramProfile, on_delete=models.CASCADE, verbose_name=_("Telegram profile"))
    service_time = models.ForeignKey(ServiceWorkingTime, on_delete=models.PROTECT, verbose_name=_("Service time"),
                                     related_name="service_queues")
    date = models.DateField(verbose_name=_("Date"))
    time = models.TimeField(verbose_name=_("Time"))
    is_cancelled = models.BooleanField(default=False, verbose_name=_("Is cancelled"))

    def __str__(self):
        return self.service.title

    class Meta:
        verbose_name = _("Service queue")
        verbose_name_plural = _("Service queues")
        db_table = "service_queue"
