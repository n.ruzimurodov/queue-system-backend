from datetime import datetime

from django.db import models

from apps.common.models import BaseModel
from .organization import Organization
from django.utils.translation import gettext_lazy as _


class Service(BaseModel):
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE, verbose_name=_("Organization"),
                                     related_name="services")
    title = models.CharField(max_length=255, verbose_name=_("Service title"))
    description = models.TextField(verbose_name=_("Service description"))
    image = models.ImageField(upload_to="service", verbose_name=_("Service image"), null=True)

    def is_available_on_date(self, date: datetime.date):
        print("Ishladi", date.weekday(), self.working_times.filter(day=date.weekday()).exists())
        if not self.working_times.filter(day=date.weekday()).exists():
            return False
        # is available in this day
        print("Ishladi")
        if self.service_queues.filter(date=date).count() < self.working_times.filter(
                day=date.weekday()).aggregate(models.Sum('number_of_clients'))['number_of_clients__sum']:
            return True
        return False

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _("Service")
        verbose_name_plural = _("Services")
        db_table = "service"


class DayChoices(models.IntegerChoices):
    MONDAY = 0, _('Monday')
    TUESDAY = 1, _('Tuesday')
    WEDNESDAY = 2, _('Wednesday')
    THURSDAY = 3, _('Thursday')
    FRIDAY = 4, _('Friday')
    SATURDAY = 5, _('Saturday')
    SUNDAY = 6, _('Sunday')


class ServiceWorkingTime(BaseModel):
    service = models.ForeignKey(Service, on_delete=models.CASCADE, verbose_name=_("Service"),
                                related_name="working_times")
    day = models.SmallIntegerField(verbose_name=_("Day"), choices=DayChoices.choices)
    start_time = models.TimeField(verbose_name=_("Start time"))
    end_time = models.TimeField(verbose_name=_("End time"), null=True, blank=True)
    duration = models.DurationField(verbose_name=_("Duration"), null=True, blank=True)
    number_of_clients = models.SmallIntegerField(verbose_name=_("Number of clients"), default=1)

    def __str__(self):
        return f"{self.service.title} - {self.day} - {self.start_time} - {self.end_time}"

    class Meta:
        verbose_name = _("Service working time")
        verbose_name_plural = _("Service working times")
        db_table = "service_working_time"
