from django.db import models
from apps.common.models import BaseModel, District
from django.utils.translation import gettext_lazy as _


class Organization(BaseModel):
    title = models.CharField(max_length=255, verbose_name=_("Organization title"))
    district = models.ForeignKey(District, on_delete=models.SET_NULL, verbose_name=_("Organization district"),
                                 null=True)
    description = models.TextField(verbose_name=_("Organization description"))
    address = models.CharField(max_length=255, verbose_name=_("Organization address"))
    phone = models.CharField(max_length=255, verbose_name=_("Organization phone"))
    email = models.EmailField(verbose_name=_("Organization email"))
    logo = models.ImageField(upload_to="organization", verbose_name=_("Organization logo"))

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _("Organization")
        verbose_name_plural = _("Organizations")
        db_table = "organization"


class OrganizationImage(BaseModel):
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE, verbose_name=_("Organization"))
    image = models.ImageField(upload_to="organization", verbose_name=_("Organization image"))

    def __str__(self):
        return self.organization.title

    class Meta:
        verbose_name = _("Organization image")
        verbose_name_plural = _("Organization images")
        db_table = "organization_image"


