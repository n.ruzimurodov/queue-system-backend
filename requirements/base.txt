django==4.2.1
psycopg2-binary==2.9.6
python-environ==0.4.54
django-cors-headers==4.0.0
django-jazzmin==2.6.0
drf-yasg==1.21.5
djangorestframework==3.14.0
django-filter==23.2
pillow==9.5.0
python-telegram-bot==13.15  # for telegram bot
sentry-sdk==1.24.0
django-rosetta==0.9.9